;;; mpasm-keywords.el

;;; Code:

(defconst mpasm-mode-keywords-control-directives ;
  '("#define" "#undefine" " #include" "end" "equ" "org" "processor" "radix" "set"
    "constant" "variable"))
;;
(defconst mpasm-mode-keywords-conditional-assembly ;
  '("if" "else" "ifdef" "ifndef" "endif" "while" "endw"))
;;
(defconst mpasm-mode-keywords-data ;
  '("__badram" "__badrom" "__maxram" "__maxrom" "__idlocks" "__config" "config" "da" "data" "db"
    "de" "dt" "dw" "cblock" "endc" "fill" "res"))
;;
(defconst mpasm-mode-keywords-listing ;
  '("error" "errorlevel" "list" "messg" "nolist" "page" "space" "subtitle" "title"))
;;
(defconst mpasm-mode-keywords-macros ;
  '("macro" "endm" "exitm" "local" "expand" "noexpand"))
;;
(defconst mpasm-mode-keywords-object-file-directives ;
  '("access_ovr" "bankisel" "banksel" "code" "code_pack" "extern" "global" "idata" "idata_acs"
    "pagesel" "pageselw" "udata" "udata_acs" "udata_ovr" "udata_shr"))
;;--------------------------------------------------------------------------------------------------
;; 14-bit instruction set
;;
(defconst mpasm-mode-keywords-named-registers '("bsr" "option" "pcl" "pch" "pclath" "pclatu" "prodh"
                                                "prodl" "tblath" "tblatl" "tblptr" "wreg")
  "MPASM named registers")
(defconst mpasm-mode-keywords-named-bits '("c" "dc" "z" "ov" "n" "to" "pd" "gie")
  "MPASM named bits")
(defconst mpasm-mode-keywords-named-features '("pc" "tos" "wdt")
  "MPASM named features")
(defconst mpasm-mode-keywords-byte-oriented '("addwf" "andwf" "clrf" "clrw" "comf" "decf" "decfsz" "incf"
                                              "iorwf" "movf" "movwf" "nop" "rlf" "rrf" "subwf"
                                              "swapf" "xorwf"))
(defconst mpasm-mode-keywords-bit-oriented '("bcf" "bsf" "btfsc" "btfss"))
(defconst mpasm-mode-keywords-literal-control '("addlw" "andlw" "call" "clrwdt" "goto" "iorlw"
                                                "movlw" "option" "retfie" "retlw" "return"
                                                "sleep""sublw" "tris" "xorlw"))
(defconst mpasm-mode-keywords-pseudo '("addcf" "adddcf" "b" "bc" "bdc" "bnc" "bndc" "bnz" "bz"
                                       "clrc" "clrdc" "clrz" "lcall" "lgoto" "movfw" "negf" "setc"
                                       "setdc" "setz" "skpc" "skpdc" "skpnc" "skpndc" "skpz"))
;;--------------------------------------------------------------------------------------------------
;; 16-bit instruction set
;;

(defconst mpasm-mode-keywords-pic18-named-registers '("bsr" "fsr" "pcl" "pch"  "pclath" "pclatu"
                                                      "prodh" "prodl" "status" "tablat" "tblptr"
                                                      "wreg"))

(defconst mpasm-mode-keywords-pic18-named-bits '("c" "dc" "z" "ov" "n" "to" "pd" "peie" "gie" "giel"
                                                 "gieh"))

(defconst mpasm-mode-keywords-pic18-named-features '("mclr" "pc" "tos" "wdt"))

(defconst mpasm-mode-keywords-pic18-byte-oriented '("addwf" "addwfc" "andwf" "clrf" "comf" "cpfseq"
                                                    "cpfsgt" "cpfslt" "decf" "decfsz" "dcfsnz"
                                                    "incf" "incfsz" "infsnz" "iorwf" "movf" "movff"
                                                    "movwf" "mulwf" "negf" "rlcf" "rlncf" "rrcf"
                                                    "rrncf" "setf" "subfwb" "subwf" "subwfb" "swapf"
                                                    "tstfsz" "xorwf"))

(defconst mpasm-mode-keywords-pic18-bit-oriented '("bcf" "bsf" "btfsc" "btfss" "btg"))

(defconst mpasm-mode-keywords-pic18-control-operations '("bc" "bn" "bnc" "bnn" "bnov" "bnz" "bov"
                                                         "bra" "bz" "call" "clrwdt" "daw" "goto"
                                                         "nop" "pop" "push" "rcall" "reset" "retfie"
                                                         "return" "sleep"))

(defconst mpasm-mode-keywords-pic18-literal-operations '("andlw" "addlw" "iorlw" "lfsr" "movlb"
                                                         "movlw" "mullw" "retlw" "sublw" "xorlw"))

(defconst mpasm-mode-keywords-pic18-memory-operations '("tblrd*" "tblrd*+" "tblrd*-" "tblrd+*"
                                                        "tblwt*" "tblwt*+" "tblwt*-" "tblwt+*"))

(defconst mpasm-mode-keywords-pic18-extended-instructions  '("addfsr" "addulnk" "callw" "movsf"
                                                             "movss" "pushl" "subfsr" "subulnk"))
;; -------------------------------------------------------------------------------------------------
;;
;;
(defconst mpasm-mode-keywords-preprocessor (append ;;
                                            mpasm-mode-keywords-control-directives
                                            mpasm-mode-keywords-conditional-assembly
                                            mpasm-mode-keywords-data
                                            mpasm-mode-keywords-listing
                                            mpasm-mode-keywords-macros
                                            mpasm-mode-keywords-object-file-directives))
;;
(defconst mpasm-mode-keywords-mnemonics (append ;; 14 bit instructions
                                         mpasm-mode-keywords-byte-oriented
                                         mpasm-mode-keywords-bit-oriented
                                         mpasm-mode-keywords-literal-control
                                         mpasm-mode-keywords-pseudo))
;;
(defconst mpasm-mode-keywords-mnemonics-pic18 (append ;; 16 bit instructions pic18
                                               mpasm-mode-keywords-pic18-byte-oriented
                                               mpasm-mode-keywords-pic18-bit-oriented
                                               mpasm-mode-keywords-pic18-control-operations
                                               mpasm-mode-keywords-pic18-extended-instructions
                                               mpasm-mode-keywords-pic18-literal-operations
                                               mpasm-mode-keywords-pic18-memory-operations ))
;;
(defconst mpasm-mode-keywords-mnemonics-all (append ;;
                                             mpasm-mode-keywords-mnemonics
                                             mpasm-mode-keywords-mnemonics-pic18))
;;
(defconst mpasm-mode-keywords-all (append
                                   mpasm-mode-keywords-preprocessor
                                   mpasm-mode-keywords-mnemonics
                                   mpasm-mode-keywords-mnemonics-pic18))
;;--------------------------------------------------------------------------------------------------
;; Redix RegExp
;;
(defconst mpasm-radix-binary-re "\\([bB]'[01]\\{8\\}'\\)") ;
(defconst mpasm-radix-octal-re "\\([oO]'[0-7]+'\\)")
(defconst mpasm-radix-decimal-re "\\([dD]'[0-9]+'\\|\\.[0-9]+\\|[0-9]+\\)")
(defconst mpasm-radix-hexa-re "\\([hH]'[0-9a-f]+'\\|0x[0-9a-f]+\\)")
(defconst mpasm-radix-ascii-re "\\([aA]'[a-z0-9]+\\|'[a-z0-9]'+\\)")
(defconst mpasm-radix-re (concat "\\(" mpasm-radix-ascii-re
                                 "\\|" mpasm-radix-binary-re
                                 "\\|" mpasm-radix-decimal-re
                                 "\\|" mpasm-radix-hexa-re
                                 "\\|" mpasm-radix-octal-re "\\)" ))
;;--------------------------------------------------------------------------------------------------
;; Arithmetic Operators
(defconst mpasm-arithmetic-operators '("$" "(" ")" "!" "-" "~" "low" "high" "upper" "*" "/" "%" "+"
                                       "-" "<<" ">>" ">=" ">" "<" "<=" "==" "!=" "&" "^" "|" "&&"
                                       "||" "=" "+=" "-=" "*=" "/=" "%=" "<<=" ">>=" "&=" "|=" "^="
                                       "++" "--" )
  "MPASM Arithmetic Operators")
;;--------------------------------------------------------------------------------------------------


(defconst mpasm-mode-registers
  '("indf" "tmr0" "pcl" "status" "fsr" "porta" "portb" "portc" "portd" "porte" "pclath" "intcon"
  "pir1" "pir2" "tmr1" "tmr1l" "tmr1h" "t1con" "tmr2" "t2con" "sspbuf" "sspcon" "ccpr1" "ccpr1l"
  "ccpr1h" "ccp1con" "rcsta" "txreg" "rcreg" "ccpr2" "ccpr2l" "ccpr2h" "ccp2con" "adresh" "adcon0"
  "option_reg" "trisa" "trisb" "trisc" "trisd" "trise" "pie1" "pie2" "pcon" "sspcon2" "pr2" "sspadd"
  "sspstat" "txsta" "spbrg" "cmcon" "cvrcon" "adresl" "adcon1" "eedata" "eeadr" "eedath" "eeadrh"
  "eecon1" "eecon2"))

(defconst mpasm-mode-bit-names
  '( "c" "dc" "z" "not_pd" "not_to" "irp" "rp0" "rp1" "ra0" "ra1" "ra2" "ra3" "ra4" "ra5" "rb0"
  "rb1" "rb2" "rb3" "rb4" "rb5" "rb6" "rb7" "rc0" "rc1" "rc2" "rc3" "rc4" "rc5" "rc6" "rc7" "rd0"
  "rd1" "rd2" "rd3" "rd4" "rd5" "rd6" "rd7" "re0" "re1" "re2" "rbif" "intf" "tmr0if" "rbie" "inte"
  "tmr0ie" "peie" "gie" "t0if" "t0ie" "tmr1if" "tmr2if" "ccp1if" "sspif" "txif" "rcif" "adif"
  "pspif" "ccp2if" "bclif" "eeif" "cmif" "tmr1on" "tmr1cs" "not_t1sync" "t1oscen" "t1sync" "t1ckps0"
  "t1ckps1" "t1insync" "tmr2on" "t2ckps0" "t2ckps1" "toutps0" "toutps1" "toutps2" "toutps3" "ckp"
  "sspen" "sspov" "wcol" "sspm0" "sspm1" "sspm2" "sspm3" "ccp1y" "ccp1x" "ccp1m0" "ccp1m1" "ccp1m2"
  "ccp1m3" "rx9d" "oerr" "ferr" "adden" "cren" "sren" "rx9" "spen" "rcd8" "rc9" "not_rc8" "rc8_9"
  "ccp2y" "ccp2x" "ccp2m0" "ccp2m1" "ccp2m2" "ccp2m3" "adon" "go_not_done" "go" "chs0" "chs1" "chs2"
  "adcs0" "adcs1" "not_done" "go_done" "psa" "t0se" "t0cs" "intedg" "not_rbpu" "ps0" "ps1" "ps2"
  "trisa0" "trisa1" "trisa2" "trisa3" "trisa4" "trisa5" "trisb0" "trisb1" "trisb2" "trisb3" "trisb4"
  "trisb5" "trisb6" "trisb7" "trisc0" "trisc1" "trisc2" "trisc3" "trisc4" "trisc5" "trisc6" "trisc7"
  "trisd0" "trisd1" "trisd2" "trisd3" "trisd4" "trisd5" "trisd6" "trisd7" "trise0" "trise1" "trise2"
  "pspmode" "ibov" "obf" "ibf" "tmr1ie" "tmr2ie" "ccp1ie" "sspie" "txie" "rcie" "adie" "pspie"
  "ccp2ie" "bclie" "eeie" "cmie" "not_bor" "not_por" "not_bo" "sen" "rsen" "pen" "rcen" "acken"
  "ackdt" "ackstat" "gcen" "bf" "ua" "r_not_w" "s" "p" "d_not_a" "cke" "smp" "r" "d" "i2c_read"
  "i2c_start" "i2c_stop" "i2c_data" "not_w" "not_a" "not_write" "not_address" "r_w" "d_a"
  "read_write" "data_address" "tx9d" "trmt" "brgh" "sync" "txen" "tx9" "csrc" "txd8" "not_tx8"
  "tx8_9" "cis" "c1inv" "c2inv" "c1out" "c2out" "cm0" "cm1" "cm2" "cvrr" "cvroe" "cvren" "cvr0"
  "cvr1" "cvr2" "cvr3" "adcs2" "adfm" "pcfg0" "pcfg1" "pcfg2" "pcfg3" "rd" "wr" "wren" "wrerr"
  "eepgd"))



(provide 'mpasm-keywords)

;; mpasm-keywords ends here

