;;; mpasm-indent.el

;;; Code:

(require 'mpasm-keywords)

(defcustom mpasm-indent-column-preproc 4
  "Number of column to indent mnemonic"
  :type 'integer
  :group 'mpasm)

(defcustom mpasm-indent-column-mnemonic 8
  "Number of column to indent mnemonic"
  :type 'integer
  :group 'mpasm)

(defcustom mpasm-indent-column-arg 20
  "Number of column to indent argument"
  :type 'integer
  :group 'mpasm)

(defun mpasm-indent-to-column-preproc ()
  "Indent preprocessor to `mpasm-indent-column-preproc' "
  (delete-horizontal-space)
  (indent-to mpasm-indent-column-preproc))

(defun mpasm-indent-to-column-mnemonic ()
  "Indent mnemonic to `mpasm-indent-column-mnemonic' "
  (delete-horizontal-space)
  (indent-to mpasm-indent-column-mnemonic))

(defun mpasm-indent-to-column-arg ()
  "Indent argument to `mpasm-indent-column-arg' "
  (delete-horizontal-space)
  (indent-to mpasm-indent-column-arg))

(defun mpasm-verbose (string)
  (when mpasm-mode-verbose
    (message (concat "[DEBUG][L:%d][C:%d]: " string) (line-number-at-pos)(current-column))))

(defun mpasm-mode-comment ()
  "Insere um comentário na linha
Classes de comentários:
 1 - (;;;) comentário na coluna de label, que por definição deve ser a coluna 0.
 2 -  (;;) comentário na coluna de mnemonicos definido pela variável `mpasm-indent-column-mnemonic'.
 3 -   (;) comentário na coluna de comentário definido pela variável `mpasm-indent-column-comment'.
"
  (interactive)
  (comment-normalize-vars)
  (let (comempty comment)
    (save-excursion
      (beginning-of-line)
      (with-no-warnings
	(setq comment (comment-search-forward (line-end-position) t)))
      (setq comempty (looking-at "[ \t]*$")))
    
    (cond
     ;; Blank line?  Then start comment at code indent level.
     ;; Just like `comment-dwim'.  -stef
     ((save-excursion (beginning-of-line) (looking-at "^[ \t]*$"))
      ;;(indent-according-to-mode)
      (mpasm-indent-to-column-mnemonic)
      (insert ";; "))

     ;; Nonblank line w/o comment => start a comment at comment-column.
     ;; Also: point before the comment => jump inside.
     ((or (null comment) (< (point) comment))
      (indent-for-comment))

     ;; Flush-left or non-empty comment present => just insert character.
     ((or (not comempty) (save-excursion (goto-char comment) (bolp)))
      (insert ";"))

     ;; Empty code-level comment => upgrade to next comment level.
     ((save-excursion (goto-char comment) (skip-chars-backward " \t") (bolp))
      (goto-char comment)
      (insert ";")
      (indent-for-comment))

     ;; Empty comment ends non-empty code line => new comment above.
     (t
      (goto-char comment)
      (skip-chars-backward " \t")
      (delete-region (point) (line-end-position))
      (beginning-of-line) (insert "\n") (backward-char)
      (mpasm-mode-comment)))))
;;
;; -------------------------------------------------------------------------------------------------
(defun mpasm-mode-indent-line ()
  "Auto-indent the current line.
"
  (interactive) 
  (cond
   ;; Linha Vazia
   ((save-excursion (beginning-of-line) (looking-at "^[ \t]*$"))
    (forward-line 0)
    (mpasm-indent-to-column-mnemonic))
   ;; Outro
   (t
    (tab-to-tab-stop)))
  ;; Verifica por comentário
  (when (save-excursion (beginning-of-line) (search-forward ";" (line-end-position) t))
    (save-excursion (mpasm-indent-comment)))
  )


(defun mpasm-indent-comment ()
  "Indenta comentários."
  (interactive)
  ;;
  (cond
   ;;
   ((save-excursion (beginning-of-line) (search-forward ";;;" (line-end-position) t))
    (mpasm-verbose "mpasm-indent-comment: Comentario ;;;")
    (indent-to-left-margin))
   ;;
   ((save-excursion (beginning-of-line) (search-forward ";;" (line-end-position) t))
    (mpasm-verbose "mpasm-indent-comment: Comentario ;;")
    (save-excursion
      (beginning-of-line) (mpasm-indent-to-column-mnemonic)))
   ;;
   ((save-excursion (beginning-of-line) (search-forward ";" (line-end-position) t))
    (mpasm-verbose "mpasm-indent-comment: Comentario ;")
    (indent-for-comment))
   ;;
   (t
    (mpasm-verbose "mpasm-indent-comment: Nenhuma condicao"))))

;; -------------------------------------------------------------------------------------------------
;; Funcoes de indentacao extra

(defun mpasm-extra-indent-multiple-args ()
  "Indent multiple arguments separated by commas (,).
<pt-br> Indenta argumentos de funcoes separados por virgulas (,)."
  (save-excursion
    (while (search-forward "," (line-end-position) t)
      (backward-char 1)
      (delete-horizontal-space)
      (forward-char 1)
      (delete-horizontal-space)
      (insert " "))))

;;;
(defun mpasm-extra-indent-keyword ()
  ""
  (interactive)
  (save-excursion
    (beginning-of-line)
    (cond
     ;; Mnemonico comum com argumento ou não
     ((looking-at (concat "^[ \t]*" (regexp-opt  mpasm-mode-keywords-mnemonics-all 'words)
			  "[ \t]*.*$"))
      (mpasm-verbose "Mnemonic found")
      (beginning-of-line) (mpasm-indent-to-column-mnemonic) (forward-word 1)
      (mpasm-verbose "Mnemonic found: check for arg")
      (when (looking-at "[ \t]*[a-zA-Z0-9._$(]+.*$")
	(mpasm-verbose "Mnemonic found: arg found")
	(mpasm-indent-to-column-arg))
      )

     ;; Preprocessors as Mnemonics (endm é provisorio)
     ((looking-at (concat "^[ \t]*" (regexp-opt '("pagesel" "banksel" "bankisel" "endm") 'words)
			  "[ \t]*.*$"))
      (mpasm-verbose "Preprocessor: Object file directives found")
      (beginning-of-line) (mpasm-indent-to-column-mnemonic) (forward-word 1)
      (when (looking-at "[ \t]*[a-zA-Z0-9._$(]+.*$")
	(mpasm-verbose "Preprocessor: Object file directives: arg found")
	(mpasm-indent-to-column-arg))
      )

     ;; Indenta o include
     ((looking-at (concat "^[ \t]*" (regexp-opt '("#include" "include" "title" "list") 'words)
                          "[ \t]*.*$"))
      (mpasm-verbose "Preprocessor #include")
      (beginning-of-line) (mpasm-indent-to-column-preproc) (forward-word 1)
      (when (looking-at "[ \t]*[\"<a-zA-Z]+.*$")
	(mpasm-verbose "#include: arg found")
        (delete-horizontal-space) (indent-to (+ mpasm-indent-column-preproc 16))
        (forward-word 1)))
     
     ;; Indenta o #define e #undefined
     ((looking-at (concat "^[ \t]*" (regexp-opt '("#define" "#undefine") 'words) "[ \t]*.*$"))
      (mpasm-verbose "Preprocessor #define")
      (beginning-of-line) (mpasm-indent-to-column-preproc) (forward-word 1)
      (when (looking-at "[ \t]*[a-zA-Z._]+.*$")
	(mpasm-verbose "Mnemonic found: arg found")
        (delete-horizontal-space) (indent-to (+ mpasm-indent-column-preproc 8))
        (forward-word 1)
        (when (looking-at "[ \t]*[a-zA-Z0-9._]+.*$")
          (mpasm-verbose "Mnemonic found: arg found")
          (delete-horizontal-space) (indent-to (+ mpasm-indent-column-preproc 32))
          (forward-word 1))))

     ;; Indenta as diretrizes de conticionamento de compilacao (assembly conditionals)
     ;; Indenta o else, if, ifdef, ifndef, endif
     ((looking-at (concat "^[ \t]*"
                          (regexp-opt '("if" ".if" "#if" "ifdef" "#ifdef" "ifndef" "#ifndef" "else"
                                        ".else" "#else" "endif" ".endif" "#endif" ".fi" "while"
                                        ".while" "endw" ".endw") 'words) "[ \t]*.*$"))
      (mpasm-verbose "Preprocessor Assembly Conditionals")
      (beginning-of-line) (mpasm-indent-to-column-preproc) (forward-word 1)
      (when (looking-at "[ \t]*[a-zA-Z._]+.*$")
	(mpasm-verbose "Mnemonic found: arg found")
        (delete-horizontal-space) (indent-to-column 12) (forward-word 1)))
     
     ;; Indenta preproc equ res code udata
     ((looking-at (concat "^[ \t]*[a-zA-Z._][a-zA-Z0-9._]*[ \t]+"
                          (regexp-opt '("equ" "res" "set" ".set"
                                        "code" "udata" "udata_shr") 'words)
                          "[ \t]*.*$"))
      (mpasm-verbose "Preprocessor: EQU SET RES CODE UDATA")
      (let ((column 32))
        (indent-to-left-margin) (beginning-of-line) (forward-word 1)
        (delete-horizontal-space) (indent-to-column column) (forward-word 1)
        (when (looking-at "[ \t]*[a-zA-Z0-9._$(]+.*$")
          (delete-horizontal-space)(indent-to (+ 8 column)))
        ))
     
     ;; Indenta o GLOBAL e EXTERN, e VARIABLE
     ((looking-at (concat "^[ \t]*" (regexp-opt '("global" "extern" "variable") 'words) "[ \t]*.*$"))
      (mpasm-verbose "Preprocessor GLOBAL EXTERN VARIABLE")
      (beginning-of-line) (mpasm-indent-to-column-preproc) (forward-word 1)
      (when (looking-at "[ \t]*[a-zA-Z._]+.*$")
	(mpasm-verbose "Mnemonic found: arg found")
        (delete-horizontal-space) (indent-to (+ mpasm-indent-column-preproc 16))
        ;; Indenta os argumentos separados por virgulas
        (mpasm-extra-indent-multiple-args)
        ))

     ;; Indenta o MACRO
     ((looking-at (concat "^[ \t]*[a-zA-Z._][a-zA-Z0-9._]*[ \t]+"
                          "\\<\\(macro\\)\\>"
                          "[ \t]*.*$"))
      (mpasm-verbose "Preprocessor MACRO")
      (indent-to-left-margin) (beginning-of-line) (forward-word 1)
      (mpasm-indent-to-column-arg) (forward-word 1)
      (when (looking-at "[ \t]*[a-zA-Z._]+.*$")
	(mpasm-verbose "MACRO Definition: Parameters found")
        (delete-horizontal-space) (indent-to (+ mpasm-indent-column-arg 8))
        ;; Indenta os argumentos separados por virgulas
        (mpasm-extra-indent-multiple-args)
        ))
     
     ;; Nenhuma das condicoes anteriores
     (t
      (mpasm-verbose "Não foi possivel indentar (extra)"))
     ))
  ;; Verifica por comentário
  (save-excursion
    (beginning-of-line)
    (when (search-forward ";" (line-end-position) t)
      (mpasm-verbose "MPASM:mpasm-extra-indent:Comment found")
      (mpasm-indent-comment)))
  ;; Remove espacos no fim da linha
  (save-excursion
    (end-of-line)
    (delete-horizontal-space))
  )


(defun mpasm-extra-indent-buffer ()
  "Indent all buffer calling mpasm-extra-indent-keyword for each line"
  (interactive)
  (save-excursion
    (beginning-of-buffer)
    (while (not (eobp))
      (mpasm-extra-indent-keyword)
      (forward-line 1))))


(provide 'mpasm-indent)

;; mpasm-indent.el ends here

